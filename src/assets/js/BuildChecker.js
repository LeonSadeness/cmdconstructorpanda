class BuildChecker {
    // список проверок
    allResult = [];
    // успех всех проверок 
    get isDone(){ return this.allResult.every(r => r.result);}

    // конструктор
    constructor(blocks) {
        this.blocks = blocks;
    }

    // добавить собственный результат 
    AddManualResult(result, msg) {
        this.allResult.push({ result, msg });
    }
    // добавить собственную проверку
    AddManualCheck(msgTrue, msgFalse, func) {
        let result = func();
        let msg = result ? msgTrue : msgFalse;
        this.allResult.push({ result, msg });
        return result;
    }
    // -----------------------------------------
    // проверка аргументов
    CheckArguments() {
        let result = this.blocks[0].consts.length > 0 ? this.blocks[0].consts.every(c => c.Valid) : true;
        let msg = result ? "аргументы в порядке" : "Ошибка в обьявлении аргументов команды";
        this.allResult.push({ result, msg });
        return result;
    }
    // проверяет наличие обязательных аргументов блока 
    CheckBlockRequiredArgs(block) {
        let indexes = block.func.value.Interchangeable;
        let result = false;
        let msg = `Входные аргументы блока - "${block.func.value.Name}" `;
        if (indexes.length > 0)
            result = indexes.some(i => block.inArgs[i].contact);
        else
            result = indexes.every(i => block.inArgs[i].contact);

        msg += result ? "в порядке" : "не установлены";
        this.allResult.push({ result, msg });
        return result;
    }

    // наличие стартого пути
    IsStartPath() {
        let result = this.blocks[0].outLogic[0].contact;
        let msg = result ? "Есть соединение со стартовым блоком" : "Отсутствует логическое соединение со стартовым блоком";
        this.allResult.push({ result, msg });
        return result;
    }
}

export default BuildChecker